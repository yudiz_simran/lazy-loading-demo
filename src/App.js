import React, { Suspense, lazy } from 'react'
import { Switch, BrowserRouter as Router, Route } from 'react-router-dom'
import { hot } from 'react-hot-loader'

import './App.css'

// Components
const Home = lazy(() => import('./Components/Home/home'))
const About = lazy(() => import('./Components/About-US/about'))
const Products = lazy(() => import('./Components/Products/Products'))

function App () {
  return (
    <>
       <Router>
           <Suspense fallback={<h1>Loading...</h1>}>
               <Switch>
                   {/* Products-Component */}
                   <Route exact component={Products} path="/products" />

                   {/* About-Us-Component */}
                   <Route exact component={About} path="/about" />

                   {/* Home-Component */}
                   <Route exact component={Home} path="/" />
               </Switch>
           </Suspense>
       </Router>
    </>
  )
}

export default hot(module)(App)
