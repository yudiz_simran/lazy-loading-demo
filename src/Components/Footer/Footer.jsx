import React from 'react'

const Footer = () => {
  return (
       <>
         {/* Footer */}
            <footer className="footer text-faded text-center py-5">
                <div className="container"><p className="m-0 small">Copyright © Nestle 2021</p></div>
            </footer>
       </>
  )
}

export default Footer
