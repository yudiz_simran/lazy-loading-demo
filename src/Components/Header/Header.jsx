import React, { useState } from 'react'
import { Link } from 'react-router-dom'

const Header = () => {
  const [toggle, setToggle] = useState(false)

  const setToggleMenu = () => {
    var element = document.getElementById('navbarSupportedContent')
    console.log('toggle', toggle)
    if (!toggle) {
      setToggle(true)
      element.classList.add('show')
      console.log('true toggle', toggle)
    } else {
      setToggle(false)
      element.classList.remove('show')
    }
  }
  return (
       <>
       {/* Header */}
       <header>
            <h1 className="site-heading text-center text-faded d-none d-lg-block">
                <span className="site-heading-lower">Nestle</span>
            </h1>
            </header>

          {/* Navbar */}
            <nav className="navbar navbar-expand-lg navbar-dark py-lg-4" id="mainNav">
              <div className="container">
                <a className="navbar-brand text-uppercase fw-bold d-lg-none">Nestle</a>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"
                 onClick={setToggleMenu}
                ><span className="navbar-toggler-icon" /></button>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav mx-auto">
                    <li className="nav-item px-lg-4"><Link className="nav-link text-uppercase" to="/">Home</Link></li>
                    <li className="nav-item px-lg-4"><Link className="nav-link text-uppercase" to="/about">About</Link></li>
                    <li className="nav-item px-lg-4"><Link className="nav-link text-uppercase" to="/products">Products</Link></li>
                    </ul>
                </div>
            </div>
            </nav>

       </>
  )
}

export default Header
